#!/bin/bash

[ -d ./public ] && rm -rf ./public

zim --index ./notebook.zim

zim --export \
    --format=html  --template=./EightFiveZero.html \
    --output=./public \
    --overwrite \
    --verbose \
    ./notebook.zim

# fix broken .txt to .html links
find ./public -name "*.html" -exec sed -i '{s/\(<a href=".*\.\)txt" title/\1html" title/g}' {} \;

# add index.html
cd public
ln -s Home.html index.html


